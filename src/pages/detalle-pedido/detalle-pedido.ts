import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

//importando plugins
import { Storage } from '@ionic/storage';

/**
 * Generated class for the DetallePedidoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-detalle-pedido',
	templateUrl: 'detalle-pedido.html'
})
export class DetallePedidoPage {
	private listPedido: any = [];
	private subTotal: number = 0;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public storage: Storage,
		public toastCtrl: ToastController
	) {}

	ionViewDidLoad() {
		this.storage.get('pedido').then((resp) => {
			this.listPedido = resp;
			if (this.listPedido != undefined) {
				for (let element of this.listPedido) {
					this.subTotal += +element.precio;
				}
			} else {
				this.mostrarAlerta('No hay productos para mostrar');
				this.navCtrl.popToRoot();
			}
		});
	}

	mostrarAlerta(message) {
		let countdown: number = 1;

		let toast = this.toastCtrl.create({
			message: message,
			duration: countdown * 1000
		});
		toast.present();
	}

	sumar(pedido) {
		pedido.cantidad += 1;
		this.subTotal += +pedido.precio;
	}

	restar(pedido) {
		if (pedido.cantidad > 0) {
			pedido.cantidad -= 1;
			this.subTotal -= +pedido.precio;
		}
	}

	continuarPedido() {
		this.navCtrl.push('DatosEnvioPage', {
			listPedido: this.listPedido,
			subTotal: this.subTotal
		});
	}

	eliminar(pedido) {
		var index = this.listPedido.indexOf(pedido, 0);
		if (index > -1) {
			this.listPedido.splice(index, 1);
		}

		this.storage.set('pedido', this.listPedido);

		if (this.listPedido.length == 0) {
			this.navCtrl.popToRoot();
		}

		console.log(this.listPedido.length);
	}
}
