import { Component, NgZone } from '@angular/core';
import {
	IonicPage,
	NavController,
	NavParams,
	AlertController,
	LoadingController,
	ModalController,
	Modal
} from 'ionic-angular';

//importando librerias
import { Storage } from '@ionic/storage';
import { AuthProvider } from '../../providers/auth/auth';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import * as Util from '../../providers/Util';
import { SelectorListContext } from '@angular/compiler';
import { useAnimation } from '@angular/core/src/animation/dsl';

declare var google;

@IonicPage()
@Component({
	selector: 'page-datos-envio',
	templateUrl: 'datos-envio.html'
})
export class DatosEnvioPage {
	private loading: any;
	private user: any = [];
	/* private direccion: any; */
	private listPedido: any = [];
	private data: any = [];
	private formasPago;
	private selecion;
	private seleccion;
	private subTotal: number;
	private iva: number = 12;
	private cargo_paypal: number = 6.6;
	private total: number;
	private cargoDomicilio: number;
	private observaciones: string;
	private factura: any;
	GoogleAutocomplete = new google.maps.places.AutocompleteService();
	autocomplete = { input: '' };
	autocompleteItems = [];
	private dir: string = '';

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		public storage: Storage,
		public auth: AuthProvider,
		public loadingCtrl: LoadingController,
		public geolocation: Geolocation,
		public geocoder: NativeGeocoder,
		public zone: NgZone,
		public modal: ModalController
	) {}

	ionViewWillEnter() {}

	ionViewDidLoad() {
		this.mostrarProgressBar('Cargando');
		this.factura = false;
		this.selecion = 1;

		this.storage.get('user').then((val) => {
			this.user = val;

			this.subTotal = this.navParams.get('subTotal');
			this.listPedido = this.navParams.get('listPedido');

			this.auth.formasdepago().then((resp) => {
				this.formasPago = resp;
			});
		});

		this.geolocation
			.getCurrentPosition()
			.then((response) => {
				this.getDireccion(response.coords.latitude, response.coords.longitude);
				this.calcularTaxa(response.coords.latitude, response.coords.longitude);
			})
			.catch((error) => {
				this.loading.dismiss();
				this.mostrarAlerta('No tiene prendido su GPS');
			});
	}

	getDireccion(lat, lng) {
		this.geocoder
			.reverseGeocode(lat, lng)
			.then((result: NativeGeocoderReverseResult[]) => {
				//.then((result: any) => {
				let address = [
					(result[0].thoroughfare || '') + ' ' + (result[0].subThoroughfare || ''),
					result[0].locality
				].join(', ');

				this.autocomplete.input = address;
				this.loading.dismiss();
			})
			.catch((error: any) => {
				this.loading.dismiss();
			});
	}
	openFactura() {
		if (this.factura == true) {
			const myModal: Modal = this.modal.create('ModalFacturaPage');

			myModal.present();

			myModal.onDidDismiss((data) => {
				console.log(data);
				this.data = data;
			});
		}
	}

	confirmarPedido() {
		//

		this.mostrarProgressBar('Cargando...');
		if (this.autocomplete.input == ' ' || this.autocomplete.input == undefined) {
			this.mostrarAlerta('Por favor rellenar todos los campos');
			this.loading.dismiss();
			return;
		}

		///
		let alert = this.alertCtrl.create({
			title: 'Aviso',
			message: 'Su forma de pago es correcta? ',
			buttons: [
				{
					text: 'Cambiar',
					role: 'cancel',
					handler: () => {
						this.loading.dismiss();
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Confirmar',
					handler: () => {
						var platillos = {};
						for (let element of this.listPedido) {
							platillos[element.id] = {
								cantidad: element.cantidad
							};
						}

						if (this.factura == false && this.selecion == 1) {
							this.auth
								.realizarOrden({
									usuario_id: this.user.id,
									whatsapp: this.user.whatsapp,
									telefonofac: this.user.telefono,
									direccion: this.autocomplete.input,
									link: this.subTotal,
									total: this.subTotal + this.cargoDomicilio,
									platillos: platillos,
									formapago: this.selecion,
									observaciones: this.observaciones,
									factura: this.factura,
									cargoDomicilio: this.cargoDomicilio
								})
								.then(
									(resp) => {
										this.loading.dismiss();
										if (resp.sucess) {
											this.storage.set('pedido', null).then(() => {
												this.mostrarAlerta('Se a realizado su orden con exito!!');
												this.navCtrl.push('DetallesHistorialPedidoPage', {
													orden: resp.orden,
													user: this.user
												});
											});
										} else this.mostrarAlerta('Ha ocurrido un error');
									},
									(error) => {
										this.mostrarAlerta('Ha ocurrido un error');
									}
								);
						} else if (this.factura == true && this.selecion == 1) {
							this.auth
								.realizarOrden({
									usuario_id: this.user.id,
									whatsapp: this.user.whatsapp,
									telefonofac: this.user.telefono,
									nombrefac: this.data.nombrefac,
									rucfac: this.data.rucfac,
									emailfac: this.data.emailfac,
									direccionfac: this.data.direccionfac,
									direccion: this.autocomplete.input,
									link: this.subTotal,
									total: this.subTotal + this.subTotal * this.iva / 100 + this.cargoDomicilio,
									platillos: platillos,
									formapago: this.selecion,
									observaciones: this.observaciones,
									factura: this.factura,
									iva: this.subTotal * this.iva / 100,
									cargoDomicilio: this.cargoDomicilio
								})
								.then(
									(resp) => {
										this.loading.dismiss();
										if (resp.sucess) {
											this.storage.set('pedido', null).then(() => {
												this.mostrarAlerta('Se a realizado su orden con exito!!');
												this.navCtrl.push('DetallesHistorialPedidoPage', {
													orden: resp.orden,
													user: this.user
												});
											});
										} else this.mostrarAlerta('Ha ocurrido un error');
									},
									(error) => {
										this.mostrarAlerta('Ha ocurrido un error');
									}
								);
						} else if (this.factura == true && this.selecion != 1) {
							this.auth
								.realizarOrden({
									usuario_id: this.user.id,
									whatsapp: this.user.whatsapp,
									telefonofac: this.user.telefono,
									nombrefac: this.data.nombrefac,
									rucfac: this.data.rucfac,
									emailfac: this.data.emailfac,
									direccionfac: this.data.direccionfac,
									direccion: this.autocomplete.input,
									total:
										this.subTotal +
										this.cargoDomicilio +
										this.subTotal * this.iva / 100 +
										(this.subTotal + this.subTotal * this.iva / 100 + this.cargoDomicilio) *
											this.cargo_paypal /
											100,
									platillos: platillos,
									formapago: this.selecion,
									observaciones: this.observaciones,
									factura: this.factura,
									iva: this.subTotal * this.iva / 100,
									cargoDomicilio: this.cargoDomicilio,
									pagoespecial:
										(this.subTotal + this.subTotal * this.iva / 100 + this.cargoDomicilio) *
										this.cargo_paypal /
										100
								})
								.then(
									(resp) => {
										this.loading.dismiss();
										if (resp.sucess) {
											this.storage.set('pedido', null).then(() => {
												this.mostrarAlerta('Se a realizado su orden con exito!!');
												this.navCtrl.push('DetallesHistorialPedidoPage', {
													orden: resp.orden,
													user: this.user
												});
											});
										} else this.mostrarAlerta('Ha ocurrido un error');
									},
									(error) => {
										this.mostrarAlerta('Ha ocurrido un error');
									}
								);
						} else if (this.factura == false && this.selecion != 1) {
							this.auth
								.realizarOrden({
									usuario_id: this.user.id,
									whatsapp: this.user.whatsapp,
									telefonofac: this.user.telefono,
									direccion: this.autocomplete.input,
									total:
										this.subTotal +
										this.cargoDomicilio +
										(this.subTotal + this.cargoDomicilio) * this.cargo_paypal / 100,
									platillos: platillos,
									formapago: this.selecion,
									observaciones: this.observaciones,
									factura: this.factura,
									cargoDomicilio: this.cargoDomicilio,
									pagoespecial: (this.subTotal + this.cargoDomicilio) * this.cargo_paypal / 100
								})
								.then(
									(resp) => {
										this.loading.dismiss();
										if (resp.sucess) {
											this.storage.set('pedido', null).then(() => {
												this.mostrarAlerta('Se a realizado su orden con exito!!');
												this.navCtrl.push('DetallesHistorialPedidoPage', {
													orden: resp.orden,
													user: this.user
												});
											});
										} else this.mostrarAlerta('Ha ocurrido un error');
									},
									(error) => {
										this.mostrarAlerta('Ha ocurrido un error');
									}
								);
						}
					}
				}
			]
		});
		alert.present();
		////
	}

	mostrarProgressBar(message: string): void {
		this.loading = this.loadingCtrl.create({
			content: message
		});
		this.loading.present();
	}

	mostrarAlerta(message) {
		let alert = this.alertCtrl.create({
			title: 'Aviso',
			subTitle: message,
			buttons: [ 'OK' ]
		});
		alert.present();
	}

	updateSearchResults() {
		if (this.autocomplete.input == '') {
			this.autocompleteItems = [];
			return;
		}
		this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input }, (predictions, status) => {
			this.autocompleteItems = [];
			this.zone.run(() => {
				predictions.forEach((prediction) => {
					this.autocompleteItems.push(prediction);
				});
			});
		});
	}

	selectSearchResult(result) {
		this.storage.set('dir', result.place_id);
		this.calcularTaxaString();
		this.autocomplete.input = result.description;
		this.autocompleteItems = [];
		this.dir = result.place_id;
	}

	calcularTaxa(lat, lng) {
		var direccion = Util.GOOGLE_DIST + 'origins=' + lat + ',' + lng + Util.GOOGLE_DESTINATIONS;
		this.auth.getDistance(direccion).then(
			(resp) => {
				var km = resp.rows[0].elements[0].distance.value / 1000;
				console.log(km);
				if (km >= 0 && km <= 7) this.cargoDomicilio = 4;
				else if (km >= 8 && km <= 8.99999) this.cargoDomicilio = 4.5;
				else if (km >= 9 && km <= 9.99999) this.cargoDomicilio = 5;
				else if (km >= 10 && km <= 10.99999) this.cargoDomicilio = 5.5;
				else if (km >= 11 && km <= 11.99999) this.cargoDomicilio = 6;
				else if (km >= 12 && km <= 12.99999) this.cargoDomicilio = 6.5;
				else if (km >= 13 && km <= 13.99999) this.cargoDomicilio = 7;
				else if (km >= 14 && km <= 14.99999) this.cargoDomicilio = 7.5;
				else if (km >= 15 && km <= 15.99999) this.cargoDomicilio = 8;
				else if (km >= 16 && km <= 16.99999) this.cargoDomicilio = 8.5;
				else if (km >= 17 && km <= 17.99999) this.cargoDomicilio = 9;
				else if (km >= 18 && km <= 18.99999) this.cargoDomicilio = 9.5;
				else if (km >= 19 && km <= 19.99999) this.cargoDomicilio = 10;
				else if (km >= 20 && km <= 20.99999) this.cargoDomicilio = 10.5;
				else if (km >= 21 && km <= 21.99999) this.cargoDomicilio = 11;
				else if (km >= 22 && km <= 22.99999) this.cargoDomicilio = 11.5;
				else if (km >= 23 && km <= 23.99999) this.cargoDomicilio = 12;
				else if (km >= 24 && km <= 24.99999) this.cargoDomicilio = 12.5;
				else if (km >= 25 && km <= 25.99999) this.cargoDomicilio = 13;
				else if (km >= 26 && km <= 26.99999) this.cargoDomicilio = 13.5;
				else if (km >= 27 && km <= 27.99999) this.cargoDomicilio = 14;
				else if (km >= 28 && km <= 28.99999) this.cargoDomicilio = 14.5;
				else if (km >= 29 && km <= 29.99999) this.cargoDomicilio = 15;
				else if (km >= 30 && km <= 30.99999) this.cargoDomicilio = 15.5;
				else if (km >= 31 && km <= 31.99999) this.cargoDomicilio = 16;
				else if (km >= 32 && km <= 32.99999) this.cargoDomicilio = 16.5;
				else if (km >= 33 && km <= 33.99999) this.cargoDomicilio = 17;
				else if (km >= 34 && km <= 34.99999) this.cargoDomicilio = 17.5;
				else if (km >= 35 && km <= 35.99999) this.cargoDomicilio = 18;
				else if (km >= 36) {
					console.log('no aplica');
					this.mostrarAlerta(
						'La distancia de tu ubicacion  no esta contemplada en nuestro rango de servicio'
					);
				}
			},
			(error) => alert(JSON.stringify(error))
		);
	}

	calcularTaxaString() {
		this.storage.get('dir').then((resp) => {
			let place = resp;
			console.log(place);
			let direccion = Util.GOOGLE_DIST + 'origins=place_id:' + place + Util.GOOGLE_DESTINATIONS;
			console.log(direccion);
			this.auth.getDistance(direccion).then(
				(resp) => {
					var km = resp.rows[0].elements[0].distance.value / 1000;
					console.log(km);
					if (km >= 0 && km <= 7) this.cargoDomicilio = 4;
					else if (km >= 8 && km <= 8.99999) this.cargoDomicilio = 4.5;
					else if (km >= 9 && km <= 9.99999) this.cargoDomicilio = 5;
					else if (km >= 10 && km <= 10.99999) this.cargoDomicilio = 5.5;
					else if (km >= 11 && km <= 11.99999) this.cargoDomicilio = 6;
					else if (km >= 12 && km <= 12.99999) this.cargoDomicilio = 6.5;
					else if (km >= 13 && km <= 13.99999) this.cargoDomicilio = 7;
					else if (km >= 14 && km <= 14.99999) this.cargoDomicilio = 7.5;
					else if (km >= 15 && km <= 15.99999) this.cargoDomicilio = 8;
					else if (km >= 16 && km <= 16.99999) this.cargoDomicilio = 8.5;
					else if (km >= 17 && km <= 17.99999) this.cargoDomicilio = 9;
					else if (km >= 18 && km <= 18.99999) this.cargoDomicilio = 9.5;
					else if (km >= 19 && km <= 19.99999) this.cargoDomicilio = 10;
					else if (km >= 20 && km <= 20.99999) this.cargoDomicilio = 10.5;
					else if (km >= 21 && km <= 21.99999) this.cargoDomicilio = 11;
					else if (km >= 22 && km <= 22.99999) this.cargoDomicilio = 11.5;
					else if (km >= 23 && km <= 23.99999) this.cargoDomicilio = 12;
					else if (km >= 24 && km <= 24.99999) this.cargoDomicilio = 12.5;
					else if (km >= 25 && km <= 25.99999) this.cargoDomicilio = 13;
					else if (km >= 26 && km <= 26.99999) this.cargoDomicilio = 13.5;
					else if (km >= 27 && km <= 27.99999) this.cargoDomicilio = 14;
					else if (km >= 28 && km <= 28.99999) this.cargoDomicilio = 14.5;
					else if (km >= 29 && km <= 29.99999) this.cargoDomicilio = 15;
					else if (km >= 30 && km <= 30.99999) this.cargoDomicilio = 15.5;
					else if (km >= 31 && km <= 31.99999) this.cargoDomicilio = 16;
					else if (km >= 32 && km <= 32.99999) this.cargoDomicilio = 16.5;
					else if (km >= 33 && km <= 33.99999) this.cargoDomicilio = 17;
					else if (km >= 34 && km <= 34.99999) this.cargoDomicilio = 17.5;
					else if (km >= 35 && km <= 35.99999) this.cargoDomicilio = 18;
					else if (km >= 36) {
						console.log('no aplica');
						this.mostrarAlerta(
							'La distancia de esta  direccion no esta contemplada en nuestro rango de servicio'
						);
					}
				},
				(error) => alert(JSON.stringify(error))
			);
		});
	}
}
