import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalFacturaPage } from './modal-factura';

@NgModule({
  declarations: [
    ModalFacturaPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalFacturaPage),
  ],
})
export class ModalFacturaPageModule {}
