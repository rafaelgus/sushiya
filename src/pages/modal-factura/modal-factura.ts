import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ModalFacturaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-modal-factura',
	templateUrl: 'modal-factura.html'
})
export class ModalFacturaPage {
	formgroup: FormGroup;
	nombrefac: AbstractControl;
	direccionfac: AbstractControl;
	telefonofac: AbstractControl;
	emailfac: AbstractControl;
	rucfac: AbstractControl;

	constructor(
		public navParams: NavParams,
		public view: ViewController,
		public storage: Storage,
		public alertCtrl: AlertController,
		public formBuilder: FormBuilder
	) {
		this.formgroup = formBuilder.group({
			nombrefac: [
				this.user.name,
				Validators.compose([ Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required ])
			],
			direccionfac: [
				this.user.direccion,
				Validators.compose([ Validators.maxLength(60), Validators.required ])
			],
			telefonofac: [ this.user.telefono, Validators.required ],
			emailfac: [
				this.user.email,
				Validators.compose([
					Validators.required,
					Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
				])
			],
			rucfac: [ this.user.ruc, Validators.required ]
		});

		this.nombrefac = this.formgroup.controls['nombrefac'];
		this.direccionfac = this.formgroup.controls['direccionfac'];
		this.telefonofac = this.formgroup.controls['telefonofac'];
		this.emailfac = this.formgroup.controls['emailfac'];
		this.rucfac = this.formgroup.controls['rucfac'];
	}

	private user: any = [];
	/* 	private nombrefac: string;
	private rucfac: string;
	private telefonofac: string;
	private emailfac: string;
	private direccionfac: string; */

	ionViewDidLoad() {
		this.mostrarAlerta('Por favor completar los campos para emitir la factura');
		this.storage.get('user').then((val) => {
			this.user = val;
		});
	}

	closeModal() {
		const data = {
			nombrefac: this.user.name,
			rucfac: this.user.ruc,
			telefonofac: this.user.telefono,
			emailfac: this.user.email,
			direccionfac: this.user.direccion
		};
		this.mostrarAlerta('Datos de Facturacion guardados correctamente');
		this.view.dismiss(data);
	}

	mostrarAlerta(message) {
		let alert = this.alertCtrl.create({
			title: 'Aviso',
			subTitle: message,
			buttons: [ 'OK' ]
		});
		alert.present();
	}

	backModal() {
		this.view.dismiss();
	}
}
