import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, PopoverController } from 'ionic-angular';

//importando providers o librerias
import * as Util from '../../providers/Util';
import { AuthProvider } from '../../providers/auth/auth';
import { Storage } from '@ionic/storage';
import { PopinfoComponent } from '../../components/popinfo/popinfo';

/**
 * Generated class for the CategoriaDetallePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-categoria-detalle',
	templateUrl: 'categoria-detalle.html'
})
export class CategoriaDetallePage {
	private SERVER: string = Util.SERVER;
	private myInput: string;
	private platillos: any = [];
	private listPedido: any = [];
	private skip: number = 0;

	constructor(
		public auth: AuthProvider,
		public navParams: NavParams,
		public navCtrl: NavController,
		public storage: Storage,
		public popoverCtrl: PopoverController,
		public toastCtrl: ToastController
	) {}

	ionViewDidLoad() {
		this.storage.get('pedido').then((resp) => {
			if (resp != null) this.listPedido = resp;

			this.auth.categoria(this.navParams.get('id')).then((resp) => {
				if (resp.response.sucess == 1) {
					this.filtro(resp.response.platillos);
				}
			});
		});
	}

	onInput() {
		this.platillos = [];
		if (this.myInput != '') {
			this.auth.busquedaPorCategoria(this.myInput, this.navParams.get('id')).then((resp) => {
				console.log(resp);
				this.filtro(resp.response.platillos);
			});
		} else {
			this.auth.categoria(this.navParams.get('id')).then((resp) => {
				if (resp.response.sucess == 1) {
					this.filtro(resp.response.platillos);
				}
			});
		}
	}

	agregar(plato) {
		plato.cantidad = 1;
		this.listPedido.push(plato);
		var index = this.platillos.indexOf(plato, 0);
		if (index > -1) {
			this.platillos.splice(index, 1);
		}

		this.storage.set('pedido', this.listPedido);
	}

	pedido() {
		this.navCtrl.push('DetallePedidoPage', {
			listPedido: this.listPedido
		});
	}

	filtro(platillos) {
		let isAgregado: boolean = false;

		for (let item of platillos) {
			isAgregado = false;

			if (this.listPedido != null) {
				for (let plato of this.listPedido) {
					if (item.id == plato.id) {
						isAgregado = true;
					}
				}
			}

			if (!isAgregado) this.platillos.push(item);
		}
	}

	mostarPop(myEvent, data) {
		const popover = this.popoverCtrl.create(
			PopinfoComponent,
			{
				myData: data
			},
			{
				showBackdrop: true,
				enableBackdropDismiss: true,
				cssClass: 'mypopover'
			}
		);

		popover.present({
			ev: myEvent
		});
	}

	doInfinite(infiniteScroll) {
		this.skip += 15;
		this.auth.platillos(this.skip).then((resp) => {
			let items = resp.response.platillos;

			if (items != undefined) {
				for (let item of items) {
					this.platillos.push(item);
				}
			} else {
				this.mostrarAlerta('No existen mas platillos');
			}

			infiniteScroll.complete();
		});
	}

	mostrarAlerta(message) {
		let countdown: number = 1;

		let toast = this.toastCtrl.create({
			message: message,
			duration: countdown * 1000
		});
		toast.present();
	}
}
