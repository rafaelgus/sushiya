import { Component } from '@angular/core';

/**
 * Generated class for the AcordionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'acordion',
  templateUrl: 'acordion.html'
})
export class AcordionComponent {

  text: string;

  constructor() {
    console.log('Hello AcordionComponent Component');
    this.text = 'Hello World';
  }

}
