import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
/**
 * Generated class for the PopinfoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: 'popinfo',
	templateUrl: 'popinfo.html'
})
export class PopinfoComponent {
	private myDatos: any = {};

	constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {}

	ionViewWillEnter() {
		let myData = this.navParams.get('myData');
		this.myDatos = myData;
	}
	dismiss(data) {
		this.viewCtrl.dismiss(data);
	}
}
